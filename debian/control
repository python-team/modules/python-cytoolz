Source: python-cytoolz
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>, Nilesh Patra <nilesh@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               libpython3-all-dev,
               pybuild-plugin-pyproject,
               python3-all-dev:any,
               python3-pytest <!nocheck>,
               python3-toolz,
               python3-setuptools,
               cython3:native
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cytoolz
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cytoolz.git
Homepage: https://github.com/pytoolz/cytoolz
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python3-cytoolz
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Description: Toolz in Cython: High performance functional utilities
 Cython implementation of the toolz package,  which provides high performance
 utility functions for iterables, functions, and dictionaries.
 .
 toolz is a pure Python package that borrows heavily from contemporary
 functional languages.  It is designed to interoperate seamlessly with other
 libraries including itertools, functools, and third party libraries.
 High performance functional data analysis is possible with builtin types
 like list and dict, and user-defined data structures; and low memory
 usage is achieved by using the iterator protocol and returning iterators
 whenever possible.
 .
 cytoolz implements the same API as toolz.  The main differences are
 that cytoolz is faster (typically 2-5x faster with a few spectacular
 exceptions) and cytoolz offers a C API that is accessible to other
 projects developed in Cython. Since toolz is able to process very
 large (potentially infinite) data sets, the performance increase
 gained by using cytoolz can be significant.
